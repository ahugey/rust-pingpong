# PingPong Chatbot README #

A rust _ping pong_ chatbot.

## How do I get set up? ##

* Follow steps to install [rust](https://www.rust-lang.org/tools/install)
* Clone this repository
* Execute `cargo run` in the project root directory

## TODO:

* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

## Contribution Guidelines ##

* Writing tests
* Code review
* Other guidelines

## Who do I talk to? ##

* [ahugenberg9@gmail.com](mailto:ahugenberg9@gmail.com)

