pub struct Game {
	score1: u8,
	score2: u8,
	is_singles: bool,
}

impl Game {
	fn is_game_over(&self) -> bool {
		let max_score:u8;
		if self.is_singles {
			max_score = 12;
		} else {
			max_score = 21;
		}

		let is_by_2:bool;
		if (self.score1 > self.score2 && self.score1 - self.score2 >= 2) 
			|| self.score2 - self.score1 >= 2 {
			is_by_2 = true;
		} else {
			is_by_2 = false;
		}

		if is_by_2 && 
			(self.score1 > max_score || self.score2 > max_score) {
			return true;
		}
		return false;
	}
}

fn main() {
	let new_game:Game = Game { 
		score1:0,
		score2:0,
		is_singles:true,
	};

	println!("{}",new_game.score2);
	println!("{}",new_game.score1);
	println!("{}",new_game.is_game_over());
}